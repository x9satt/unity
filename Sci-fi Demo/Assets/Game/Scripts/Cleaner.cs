﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cleaner : MonoBehaviour {

	// Use this for initialization
	void Start () {
        float time = 0;
        switch (this.gameObject.tag)
        {
            case "PickupSound" :
                time = 2.2f;
                break;
            case "HitMarker" :
                time = 0.1f;
                break;
        }
        Destroy(this.gameObject, time);
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
