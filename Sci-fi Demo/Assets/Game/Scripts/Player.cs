﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private UIManager _uiManager;

    private CharacterController _controller;
    [SerializeField]
    private float _speed = 3f;
    private float _gravity = 9.81f;

    [SerializeField]
    private GameObject _muzzleFlash;
    [SerializeField]
    private GameObject _hitMarkerPrefab;
    [SerializeField]
    private AudioSource _weaponAudio;
    [SerializeField]
    private int _currentAmmo = 0;
    private int _maxAmmo = 100;
    private bool _isReloading = false;
    [SerializeField]
    private GameObject _weapon;

    [SerializeField]
    private bool _hasCoin = false;

    

    // Use this for initialization
    void Start () {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _uiManager.UpdateAmmoInfo(_currentAmmo);
        _controller = GetComponent<CharacterController>();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        //_muzzleFlash = GameObject.Find("Muzzle_flash").gameObject;

        

    }
	
	// Update is called once per frame
	void Update () {
        
        CallculateMovement();
        CallMenu();
        ReloadHandler();

        if (Input.GetMouseButton(0) && _currentAmmo > 0 && !_isReloading)
        {
            _currentAmmo--;
            _uiManager.UpdateAmmoInfo(_currentAmmo);
            Ray ray = Camera.main.ViewportPointToRay(new Vector3 (0.5f, 0.5f, 0));
            RaycastHit hitInfo;
            _muzzleFlash.active = true;
            if (!_weaponAudio.isPlaying)
            {
                _weaponAudio.Play();
            }
            
            //Shoot();
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
            {
                Instantiate(_hitMarkerPrefab, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));

                Destructable crate = hitInfo.transform.GetComponent<Destructable>();
                if (crate)
                {
                    crate.DestroyCrate();
                }
            }
        }
        else
        {
            _muzzleFlash.active = false;
            _weaponAudio.Stop();
        }
	}

    public void ReloadHandler()
    {
        if (Input.GetKeyDown(KeyCode.R) && !_isReloading)
        {
            StartCoroutine(Reload());
        }
    }

    IEnumerator Reload()
    {
        _isReloading = true;
        yield return new WaitForSeconds(1f);
        _currentAmmo = 100;
        _uiManager.UpdateAmmoInfo(_currentAmmo);
        _isReloading = false;
    }

    public void Shoot()
    {
        _muzzleFlash.active = true;
        if (Input.GetMouseButtonUp(0))
        {
            _muzzleFlash.active = false;
        }
        //StartCoroutine(ShootCoroutine());
    }

    public IEnumerator ShootCoroutine()
    {
        yield return new WaitForSeconds(1f);
        _muzzleFlash.active = false;
    }

    void CallculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontalInput, 0, verticalInput);
        Vector3 velocity = direction * _speed;
        velocity.y -= _gravity;

        velocity = transform.transform.TransformDirection(velocity);
        _controller.Move(velocity * Time.deltaTime);
    }

    void CallMenu()
    {
        if ( Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void GetCoin()
    {
        _hasCoin = true;
        _uiManager.UpdateCoin(false);
    }

    public void RemoveCoin()
    {
        _hasCoin = false;
        _uiManager.UpdateCoin(true);
    }

    public bool CheckCoin()
    {
        if (_hasCoin == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void EnableWeapon()
    {
        _weapon.SetActive(true);
        _currentAmmo = 50;
        try
        {
            _uiManager.ShowAmmoInfo();
            _uiManager.UpdateAmmoInfo(_currentAmmo);
        }
        catch (MissingReferenceException mre)
        {
            Debug.Log("no uiManager");
        }
        
    }
}
