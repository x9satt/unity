﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [SerializeField]
    private GameObject _ammoInfo;
    [SerializeField]
    private Text _ammoInfoText;
    [SerializeField]
    private GameObject _coinImg;
	// Use this for initialization
    public void ShowAmmoInfo()
    {
        _ammoInfo.SetActive(true);
    }
	public void UpdateAmmoInfo(int count)
    {
        _ammoInfoText.text = "Ammo: " + count;
    }
    public void UpdateCoin(bool hasCoin)
    {
        if (!hasCoin)
        {
            _coinImg.SetActive(true);
        }
        else
        {
            _coinImg.SetActive(false);
        }
    }
}
