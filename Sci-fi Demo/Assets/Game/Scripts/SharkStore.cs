﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkStore : MonoBehaviour {


    [SerializeField]
    private AudioClip _sellSound;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider other)
    {
        if ( other.tag == "Player" )
        {
            if ( Input.GetKeyDown(KeyCode.E) )
            {
                Player player = other.GetComponent<Player>();
                if (player)
                {
                    if ( player.CheckCoin() )
                    {
                        player.RemoveCoin();
                        AudioSource.PlayClipAtPoint(_sellSound, transform.position, 1f);
                        player.EnableWeapon();
                    }
                    else
                    {
                        Debug.Log("You have no money!");
                    }
                }
            }
        }
    }
}
