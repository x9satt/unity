﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private GameObject[] _powerups;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(SpawnPowerupRoutine());
	}
	
    public IEnumerator SpawnEnemyRoutine()
    {
        while (true)
        {
            Instantiate(_enemyPrefab, new Vector3(Random.Range(-7.5f, 7.5f), 3.8f, 0), Quaternion.identity);
            yield return new WaitForSeconds(6f);
                      
        }              
    }

    public IEnumerator SpawnPowerupRoutine()
    {
        while (true)
        {
            int randomPowerup = Random.Range(0, 3);
            Instantiate(_powerups[randomPowerup], new Vector3(Random.Range(-7.5f, 7.5f), 3.8f, 0), Quaternion.identity);
            yield return new WaitForSeconds(7f);

        }
    }
}
