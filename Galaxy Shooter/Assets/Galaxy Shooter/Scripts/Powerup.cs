﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Powerup : MonoBehaviour {

    [SerializeField]
    private float _speed = 3.0f;
    [SerializeField]
    private int powerupID;

    [SerializeField]
    private AudioClip _clip;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        Movement();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //access the player
            Player player = other.GetComponent<Player>();
            if (player)
            {
                AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position, 1f);
                switch (powerupID)
                {
                    //enable tripple shot
                    case 0:
                        player.TrippleShotPowerupOn();
                        break;
                    //enable speed boost
                    case 1:
                        player.SpeedPowerupOn();
                        break;
                    //enable shield
                    case 2:
                        player.ShieldPowerupOn();
                        break;
                }
                
                
            }            
            //destroy powerup
            Destroy(this.gameObject);
        }        
    }

    void Movement()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);
        if (transform.position.y < -4f)
        {
            Destroy(this.gameObject);
        }
    }
}
