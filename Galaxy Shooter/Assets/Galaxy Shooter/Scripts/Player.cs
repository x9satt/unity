﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {

    public bool hasTrippleShot = false;

    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _trippleShotPrefab;
    [SerializeField]
    private GameObject _shieldPrefab;
    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private int _lives;
    [SerializeField]
    private GameObject[] _sideDamage;
    private int _side = -1;

    [SerializeField]
    private bool _hasShield = false;
    private GameObject _shield;

    [SerializeField]
    private float _fireRate = 0.25f;
    private float _nextFire = 0.0f;

    [SerializeField]
    private float _speed;

    private UIManager _uiManager;
    private AudioSource _audioSource;


    [SerializeField]
    internal bool _isFirstPlayer = true;

    // Use this for initialization
    void Start () {
        //transform.position = new Vector3(0, -3.8f, 0);
        _speed = 7f;

        _lives = 3;
        _uiManager = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>();
        if (_uiManager)
        {
            _uiManager.UpdateLivesDisplay(_lives);
        }
        _audioSource = GetComponent<AudioSource>();

        //_sideDamage[0].active = true;
        //_sideDamage[1].active = true;

        // User input
        
    }

    // Update is called once per frame
    void Update () {
        Movement();

#if UNITY_ANDROID
        if (_isFirstPlayer && Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Fire")) ;
        {
            Shoot();
        }

#else
        if (_isFirstPlayer && Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        else if (!_isFirstPlayer && Input.GetKeyDown(KeyCode.Q))
        {
            Shoot();
        }
#endif

    }

    private void Movement()
    {
        float verticalInput;
        float horizontalInput;

        if (_isFirstPlayer)
        {
            verticalInput = CrossPlatformInputManager.GetAxis("Vertical");// Input.GetAxis("Vertical");
            horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");// Input.GetAxis("Horizontal");
        }
        else
        {
            verticalInput = Input.GetAxis("Vertical_P2");
            horizontalInput = Input.GetAxis("Horizontal_P2");
        }


        transform.Translate(Vector3.up * _speed * verticalInput * Time.deltaTime);
        transform.Translate(Vector3.right * _speed * horizontalInput * Time.deltaTime);

        if (transform.position.y < -3.8f)
        {
            transform.position = new Vector3(transform.position.x, -3.8f, 0);
        }
        else if (transform.position.y > 3.8f)
        {
            transform.position = new Vector3(transform.position.x, 3.8f, 0);
        }
        else if (transform.position.x < -9.6f)
        {
            transform.position = new Vector3(9.6f, transform.position.y, 0);
        }
        else if (transform.position.x > 9.6f)
        {
            transform.position = new Vector3(-9.6f, transform.position.y, 0);
        }
    }

    private void Shoot()
    {
        if (_nextFire <= Time.time)
        {
            _audioSource.Play();
            if (hasTrippleShot)
            {
                Instantiate(_trippleShotPrefab, transform.position, Quaternion.identity);
                _nextFire = Time.time + _fireRate;
            }
            else
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 1.1f, 0), Quaternion.identity);
                _nextFire = Time.time + _fireRate;
            }
        }
    }

    public void TrippleShotPowerupOn()
    {
        hasTrippleShot = true;
        StartCoroutine(TrippleShotPowerupCoroutine());
    }

    public IEnumerator TrippleShotPowerupCoroutine()
    {
        yield return new WaitForSeconds(5.0f);
        hasTrippleShot = false;
    }

    public IEnumerator SpeedPowerupCoroutine()
    {
        yield return new WaitForSeconds(5.0f);
        _speed = 5f;
    }

    public void SpeedPowerupOn()
    {
        _speed *= 2f;
        StartCoroutine(SpeedPowerupCoroutine());
    }

    public void ShieldPowerupOn()
    {
        if (!_hasShield)
        {
            _shield = Instantiate(_shieldPrefab, transform.position, transform.rotation);
            _shield.transform.parent = this.transform;
            _hasShield = true;
        }        
    }

    private void GetSideDamage()
    {        
        switch (_lives)
        {
            case 2:
                int side = Random.Range(0, 2);     
                _side = side;
                _sideDamage[side].active = true;
                break;
            case 1:
                if( _side == 1)
                {
                    _sideDamage[0].active = true;
                }
                else
                {
                    _sideDamage[1].active = true;
                }
                break;
            default:
                _side = -1;
                _sideDamage[0].active = false;
                _sideDamage[1].active = false;
                break;
        }
    }

    public void getHit()
    {
        if (_hasShield)
        {
            _hasShield = false;
            Destroy(_shield);
        }
        else
        {

            _lives--;
            GetSideDamage();
            if (_uiManager)
            {
                _uiManager.UpdateLivesDisplay(_lives);
            }
                        
            if (_lives <= 0)
            {
                Instantiate(_explosionPrefab, transform.position, transform.rotation);
                _uiManager.GameOver();
                //Destroy(this.gameObject);
            }
            else
            {
                transform.position = new Vector3(0, -3.8f, 0);
            }
        }        
    }
}
