﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    private Animator _anim;
    private Player _player;
	// Use this for initialization
	void Start () {
        _anim = GetComponent<Animator>();
        _player = GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if ( _player._isFirstPlayer )
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _anim.SetBool("TurnLeft", true);
                _anim.SetBool("TurnRight", false);
            }
            else if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", false);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", true);
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", false);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                _anim.SetBool("TurnLeft", true);
                _anim.SetBool("TurnRight", false);
            }
            else if (Input.GetKeyUp(KeyCode.A))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", false);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", true);
            }
            else if (Input.GetKeyUp(KeyCode.D))
            {
                _anim.SetBool("TurnLeft", false);
                _anim.SetBool("TurnRight", false);
            }
        }
		
    }
}
