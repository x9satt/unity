﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    [SerializeField]
    private GameObject _playerPrefab;
    [SerializeField]
    private GameObject _playerOnePrefab;
    [SerializeField]
    private GameObject _playerTwoPrefab;
    [SerializeField]
    private GameObject _spawnManagerPrefab;

    [SerializeField]
    private Sprite[] _lives;
    [SerializeField]
    private SpriteRenderer _livesImage;


    public Text scoreText;
    public Text higherScoreText;

    private int _score;
    private int _higherScore;

    [SerializeField]
    private Image _titleImage;

    [SerializeField]
    private GameObject _buttons;

    private bool _isGameStarted = false;

    private Animator _pauseAnim;

    void Start()
    {
        _pauseAnim = GameObject.Find("Title").GetComponent<Animator>();
        _pauseAnim.updateMode = AnimatorUpdateMode.UnscaledTime;
    }

    void Update()
    {
        if (_isGameStarted && Input.GetKeyDown(KeyCode.Pause))
        {
            if ( Time.timeScale > 0)
            {
                PauseGame(false);
            }
            else if (Time.timeScale == 0)
            {
                PauseGame(true);
            }
        }
                

        if ( !_isGameStarted && Input.GetKeyDown(KeyCode.Space) )
        {
            _isGameStarted = true;
            PauseGame(true);
            if ( SceneManager.GetActiveScene().name == "Single" )
            {
                Instantiate(_playerPrefab, new Vector3(0, -3.8f, 0), Quaternion.identity);
            }
            else if ( SceneManager.GetActiveScene().name == "Co-Op" )
            {
                Instantiate(_playerOnePrefab, new Vector3(-5f, -3.8f, 0), Quaternion.identity);
                Instantiate(_playerTwoPrefab, new Vector3(5f, -3.8f, 0), Quaternion.identity);
            }
            
            Instantiate(_spawnManagerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            _titleImage.enabled = false;
            scoreText.enabled = true;
            higherScoreText.enabled = true;
            _livesImage.enabled = true;
            _score = 0;
            scoreText.text = "Score: " + _score;
            higherScoreText.text = "Best: " + PlayerPrefs.GetInt("Score", 0);
        }
        
        if ( !_isGameStarted && Input.GetKeyDown(KeyCode.Escape) )
        {
            LoadMainMenu();
        }
    }

    public void UpdateLivesDisplay(int lives)
    {
        _livesImage.sprite = _lives[lives];
    }

    public void UpdateScore(int count)
    {
        _score += count;
        scoreText.text = "Score: " + _score;
        if(_score > _higherScore)
        {
            _higherScore = _score;
            higherScoreText.text = "Best: " + _higherScore;
        }
    }

    public void GameOver()
    {
        _isGameStarted = false;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
        {
            Destroy(obj.gameObject);
        }
        Destroy(GameObject.FindGameObjectWithTag("SpawnManager").gameObject);
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(obj.gameObject);
        }
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Powerup"))
        {
            Destroy(obj.gameObject);
        }

        _titleImage.enabled = true;
        scoreText.enabled = false;
        higherScoreText.enabled = false;
        PlayerPrefs.SetInt("Score", _higherScore);
        _livesImage.enabled = false;
    }

    public void PauseGame(bool isPaused)
    {
        if (!isPaused)
        {
            Time.timeScale = 0;
            _titleImage.enabled = true;
            _buttons.SetActive(true);
            _pauseAnim.SetBool("isPaused", true);
        }
        else
        {
            Time.timeScale = 1;
            _titleImage.enabled = false;
            _buttons.SetActive(false);
            _pauseAnim.SetBool("isPaused", false);
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main menu");
        _isGameStarted = false;
    }
}
