﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanAnimations : MonoBehaviour {

    private AudioSource _audioSource;
	// Use this for initialization
	void Start () {
        _audioSource = GetComponent<AudioSource>();        
        _audioSource.Play();
        Destroy(this.gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
