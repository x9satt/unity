﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]
    private float _speed;

    [SerializeField]
    private GameObject _explosionPrefab;

    private UIManager _uiManager;

    //[SerializeField]
    //private AudioClip _clip;

    // Use this for initialization

    void Start () {
        _speed = 4f;
        _uiManager = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>();
    }
	
	// Update is called once per frame
	void Update () {
        Movement();
	}

    private void Movement()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y < -3.8f)
        {
            float randomX = Random.Range(-7.5f, 7.5f);
            transform.position = new Vector3(randomX, 3.8f, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if (player)
            {
                player.getHit();
            }
        }
    }

    public void GetHit()
    {
        Instantiate(_explosionPrefab, transform.position, transform.rotation);
        //AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position, 1f);

        if (_uiManager)
        {
            _uiManager.UpdateScore(25);
        }
    }
}
